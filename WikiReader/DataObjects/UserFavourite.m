//
//  UserFavourite.m
//  WikiReader
//
//  Created by Maksims Moisja on 09/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "UserFavourite.h"

@interface UserFavourite ()

@property (strong, nonatomic) NSString *pageTitle;
@property (strong, nonatomic) NSString *pageURL;

@end

@implementation UserFavourite

#pragma mark - Init

- (instancetype)initWithTitle:(NSString *)title pageURL:(NSString *)url {
	self = [super init];
	if (self) {
		if (title == nil) {
			self.pageTitle = @"";
		}
		else {
			self.pageTitle = title;
		}
		
		if (url == nil) {
			self.pageURL = @"";
		}
		else {
			self.pageURL = url;
		}
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	NSString *title = [aDecoder decodeObjectForKey:@"title"];
	NSString *url = [aDecoder decodeObjectForKey:@"url"];
	
	return [self initWithTitle:title pageURL:url];
}

- (void)encodeWithCoder:(NSCoder *)coder {
	[coder encodeObject:self.pageTitle forKey:@"title"];
	[coder encodeObject:self.pageURL forKey:@"url"];
}



@end
