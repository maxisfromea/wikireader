//
//  UserFavourite.h
//  WikiReader
//
//  Created by Maksims Moisja on 09/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserFavourite : NSObject <NSCoding>

@property (strong, nonatomic, readonly) NSString *pageTitle;
@property (strong, nonatomic, readonly) NSString *pageURL;

- (instancetype)initWithTitle:(NSString *)title pageURL:(NSString *)url;




@end
