//
//  FavouritesManager.h
//  WikiReader
//
//  Created by Maksims Moisja on 09/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserFavourite;

@interface FavouritesManager : NSObject

/*!
 Array of user favourite pages
 */
@property (strong, nonatomic, readonly) NSArray *arrayOfFavourites;

+ (id)sharedManager;

/*!
 Save page to favourites
 @param favouritePage page to save
 @return BOOL YES if page saved successfully, else NO
 */
- (BOOL)saveFavouritePage:(UserFavourite *)favouritePage;

/*!
 Deletes page from favourties
 @param page page to delete from favourites
 @returns BOOL YES if page wa deleted succesfully, else NO
 */
- (BOOL)deleteFavouritePage:(UserFavourite *)page;

/*!
 Loads user favourites from local storage
 */
- (void)restoreFavourites;

@end
