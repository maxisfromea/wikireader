//
//  FavouritesManager.m
//  WikiReader
//
//  Created by Maksims Moisja on 09/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "FavouritesManager.h"
#import "UserFavourite.h"

static NSString *kFileName = @"/UserFavourites.plist";

typedef enum {
	ActionTypeAdd,
	ActionTypeRemove
} ActionType;

@interface FavouritesManager ()

@property (strong, nonatomic) NSArray *arrayOfFavourites;

@end

@implementation FavouritesManager

#pragma mark - Init
#pragma mark -

+ (id)sharedManager {
	static FavouritesManager *favManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		favManager = [[FavouritesManager alloc] init];
	});
	return favManager;
}

- (instancetype)init {
	self = [super init];
	if (self) {
		[self restoreFavourites];
	}
	return self;
}


#pragma mark - Public methods
#pragma mark -

- (void)restoreFavourites {
	NSString *fullPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingString:kFileName];
	if ([[NSFileManager defaultManager] fileExistsAtPath: fullPath]) {
		id object = [NSKeyedUnarchiver unarchiveObjectWithFile:fullPath];
		if ([object isKindOfClass:[NSArray class]]) {
			self.arrayOfFavourites = (NSArray *)object;
		}
		else if ([object isKindOfClass:[UserFavourite class]]) {
			self.arrayOfFavourites = @[(UserFavourite *)object];
		}
		else {
			self.arrayOfFavourites = @[];
		}
	}
	else {
		self.arrayOfFavourites = @[];
	}
}

- (BOOL)saveFavouritePage:(UserFavourite *)favouritePage {
	if (favouritePage == nil) {
		return NO;
	}
	self.arrayOfFavourites = [self modifyArray:self.arrayOfFavourites withFavouritePage:favouritePage doAction:ActionTypeAdd];
	NSString *fullPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingString:kFileName];
	return [NSKeyedArchiver archiveRootObject:self.arrayOfFavourites toFile:fullPath];
}

- (BOOL)deleteFavouritePage:(UserFavourite *)favouritePage {
	if (favouritePage == nil) {
		return NO;
	}
	self.arrayOfFavourites = [self modifyArray:self.arrayOfFavourites withFavouritePage:favouritePage doAction:ActionTypeRemove];
	NSString *fullPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingString:kFileName];
	return [NSKeyedArchiver archiveRootObject:self.arrayOfFavourites toFile:fullPath];
}


#pragma mark - Private methods
#pragma mark - 

/*!
 Modifues array with user favourites based on action
 @param theArray array to modify
 @param userFavourite user favourite object to perform modification with
 @param action action to perform with array - add or remove user favourite object
 @returns NSArray modified array
 */
- (NSArray *)modifyArray:(NSArray *)theArray withFavouritePage:(UserFavourite *)userFavourite doAction:(ActionType)action {
	NSMutableArray *arrayTemp = [NSMutableArray arrayWithArray:theArray];
	if (action == ActionTypeAdd) {
		[arrayTemp addObject:userFavourite];
	}
	else {
		[arrayTemp removeObject:userFavourite];
	}
	return [NSArray arrayWithArray:arrayTemp];
}


@end
