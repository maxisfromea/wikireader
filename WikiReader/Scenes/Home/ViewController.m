//
//  ViewController.m
//  WikiReader
//
//  Created by Maksims Moisja on 05/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "ViewController.h"
#import "FavouritesManager.h"
#import "UserFavourite.h"
#import "FavouritesTableViewController.h"

@interface ViewController () <UIWebViewDelegate, FavouritesTableViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webViewWiki;
@property (weak, nonatomic) IBOutlet UILabel *labelErrorMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWebViewTop;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoading;

@property (strong, nonatomic) UITextField *textFieldTitle;

@property (assign, nonatomic) BOOL isErrorShowing;
@property (strong, nonatomic) NSURLRequest *requestWiki;

@property (strong, nonatomic) UIAlertController *alertControllerAction;
@property (strong, nonatomic) UIAlertController *alertControllerSave;

@end

@implementation ViewController

#pragma mark - View life cycle
#pragma mark -

- (void)viewDidLoad {
	[super viewDidLoad];
	
	NSString *cleanURL = [NSLocalizedString(@"TXT_wiki_url", @"URL for wiki to load") stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
	self.requestWiki = [NSURLRequest requestWithURL:[NSURL URLWithString: cleanURL]];
	[self.webViewWiki loadRequest: self.requestWiki];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];
}


#pragma mark - Getters
#pragma mark -

- (UIAlertController *)alertControllerAction {
	if (_alertControllerAction == nil) {
		_alertControllerAction = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
		[_alertControllerAction addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"TXT_share", @"Share") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
			UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[self.webViewWiki.request.URL] applicationActivities:nil];
			activityVC.popoverPresentationController.barButtonItem = self.navigationItem.rightBarButtonItem;
			[self presentViewController:activityVC animated:YES completion:nil];
		}]];
		
		[_alertControllerAction addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"TXT_save", @"Save") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
			[self presentViewController:self.alertControllerSave animated:YES completion:nil];
		}]];
		
		[_alertControllerAction addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"TXT_cancel", @"Cancel") style:UIAlertActionStyleCancel handler:nil]];
	}
	return _alertControllerAction;
}

- (UIAlertController *)alertControllerSave {
	if (_alertControllerSave == nil) {
		__weak ViewController *weakSelf = self;
		
		_alertControllerSave = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"TXT_add_fav", "Add to favourites") message:nil preferredStyle:UIAlertControllerStyleAlert];
		[_alertControllerSave addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
			textField.placeholder = NSLocalizedString(@"TXT_page_title", @"Page title");
			weakSelf.textFieldTitle = textField;
		}];
		
		[_alertControllerSave addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"TXT_save", @"Save") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
			NSString *title = self.textFieldTitle.text;
			NSString *url = self.webViewWiki.request.URL.relativeString;
			
			[[FavouritesManager sharedManager] saveFavouritePage:[[UserFavourite alloc] initWithTitle:title pageURL:url]];
			weakSelf.textFieldTitle.text = @"";
		}]];
		
		[_alertControllerSave addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"TXT_cancel", @"Cancel") style:UIAlertActionStyleCancel handler:nil]];
	}
	return  _alertControllerSave;
}

#pragma mark - User actions
#pragma mark -

- (IBAction)buttonRefreshPressed:(id)sender {
	[self dismissErrorMessageWithDelay:0];
	[self.webViewWiki loadRequest:self.requestWiki];
}

- (IBAction)buttonActionPressed:(id)sender {
	self.alertControllerAction.popoverPresentationController.barButtonItem = self.navigationItem.rightBarButtonItem;
	[self presentViewController:self.alertControllerAction animated:YES completion:nil];
}

#pragma mark - Helper methods
#pragma mark -

/*!
 Shows error message to user
 @param message error message
 */
- (void)showErrorWithMessage:(nullable NSString *)message {
	if (message) {
		self.labelErrorMessage.text = message;
	}
	else {
		self.labelErrorMessage.text = NSLocalizedString(@"ERR_generic", @"Generic error message");
	}
	
	CGRect frameLabel = self.labelErrorMessage.frame;
	frameLabel.size = [self.labelErrorMessage sizeThatFits:CGSizeMake(CGRectGetWidth(self.view.frame) - 40, CGFLOAT_MAX)];
	frameLabel.size.width = CGRectGetWidth(self.view.frame) - 40;
	self.labelErrorMessage.frame = frameLabel;
	self.constraintWebViewTop.constant = CGRectGetHeight(self.labelErrorMessage.frame) + 16;
	
	[UIView animateWithDuration:0.5 animations:^{
		[self.view layoutIfNeeded];
	} completion:^(BOOL finished) {
		self.isErrorShowing = YES;
	}];
}

/*!
 Dismisses error label
 @param delay delay of the animation
 */
- (void)dismissErrorMessageWithDelay:(NSTimeInterval)delay {
	if (self.constraintWebViewTop.constant != 0) {
		self.constraintWebViewTop.constant = 0;
		if (delay == 0) {
			[self.view layoutIfNeeded];
			self.isErrorShowing = NO;
		}
		else {
			[UIView animateWithDuration:0.3 delay:delay options:UIViewAnimationOptionCurveLinear animations:^{
				[self.view layoutIfNeeded];
			} completion:^(BOOL finished) {
				self.isErrorShowing = NO;
		}];
		}
	}
}

/*!
 Starts animation of activityLoading, disables bar button items
 */
- (void)startActivityLoadingAnimation {
	self.navigationItem.rightBarButtonItem.enabled = NO;
	self.navigationItem.leftBarButtonItem.enabled = NO;
	
	[self.activityLoading startAnimating];
	self.activityLoading.hidden = NO;
}

/*!
 Stops animation of activityLoading, enables bar button items
 */
- (void)stopActivityLoadingAnimation {
	self.navigationItem.rightBarButtonItem.enabled = YES;
	self.navigationItem.leftBarButtonItem.enabled = YES;
	
	[self.activityLoading stopAnimating];
}


#pragma mark - Delegate and DataSource methods
#pragma mark - UIWebViewDelegate methods

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	[self stopActivityLoadingAnimation];
	[self showErrorWithMessage:error.localizedDescription];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[self stopActivityLoadingAnimation];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
	[self startActivityLoadingAnimation];
}


#pragma mark - FavouritesTableViewControllerDelegate method

- (void)favouritesTableViewController:(FavouritesTableViewController *)favouritesVC didSelectFavouritePage:(NSString *)url {
	[self.webViewWiki loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString: url]]];
}


#pragma mark - UIContentContainerDelegate methods

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
	if (self.isErrorShowing) {
		// Change constraint of webview only if error is being showed at the moment
		CGRect frameLabel = self.labelErrorMessage.frame;
		frameLabel.size = [self.labelErrorMessage sizeThatFits:CGSizeMake(size.width - 40, CGFLOAT_MAX)];
		self.constraintWebViewTop.constant = frameLabel.size.height + 16;
	}
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.destinationViewController isKindOfClass:[FavouritesTableViewController class]]) {
		FavouritesTableViewController *favouritesVC = (FavouritesTableViewController *)segue.destinationViewController;
		favouritesVC.delegate = self;
	}
}

@end
