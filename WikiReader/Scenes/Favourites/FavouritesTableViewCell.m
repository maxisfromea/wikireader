//
//  FavouritesTableViewCell.m
//  WikiReader
//
//  Created by Maksims Moisja on 09/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "FavouritesTableViewCell.h"
#import "UserFavourite.h"

@interface FavouritesTableViewCell ()

@property (strong, nonatomic) UserFavourite *theFavourite;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelUrl;

@end

@implementation FavouritesTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUserFavourite:(UserFavourite *)favourite {
	self.theFavourite = favourite;
	
	self.labelTitle.text = self.theFavourite.pageTitle;
	self.labelUrl.text = self.theFavourite.pageURL;
}

@end
