//
//  FavouritesTableViewController.m
//  WikiReader
//
//  Created by Maksims Moisja on 09/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "FavouritesTableViewController.h"
#import "UserFavourite.h"
#import "FavouritesManager.h"
#import "FavouritesTableViewCell.h"

@interface FavouritesTableViewController ()

@property (strong, nonatomic) NSArray *arrayOfDataSource;

@end

@implementation FavouritesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.tableView.tableFooterView = [[UIView alloc] init];
	self.tableView.estimatedRowHeight = 44;
	self.tableView.rowHeight = UITableViewAutomaticDimension;
	
	self.arrayOfDataSource = [[FavouritesManager sharedManager] arrayOfFavourites];
	
	self.clearsSelectionOnViewWillAppear = YES;
	self.navigationItem.title = NSLocalizedString(@"TXT_fav_title", @"Favourites");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrayOfDataSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FavouritesTableViewCell *cell = (FavouritesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CellFavourite" forIndexPath:indexPath];
	[cell setUserFavourite:self.arrayOfDataSource[indexPath.row]];
	
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		UserFavourite *uf = self.arrayOfDataSource[indexPath.row];
		[[FavouritesManager sharedManager] deleteFavouritePage:uf];
		self.arrayOfDataSource = [[FavouritesManager sharedManager] arrayOfFavourites];
		
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (self.delegate != nil) {
		UserFavourite *uf = self.arrayOfDataSource[indexPath.row];
		[self.delegate favouritesTableViewController:self didSelectFavouritePage:uf.pageURL];
	}
	[self.navigationController popViewControllerAnimated:YES];
}

@end
