//
//  FavouritesTableViewController.h
//  WikiReader
//
//  Created by Maksims Moisja on 09/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FavouritesTableViewController;

@protocol FavouritesTableViewControllerDelegate <NSObject>

/*!
 Tells delegate that user has selected favourite page
 @param favouritesVC view controller
 @param url selected favourite url
 */
- (void)favouritesTableViewController:(FavouritesTableViewController *)favouritesVC didSelectFavouritePage:(NSString *)url;

@end

@interface FavouritesTableViewController : UITableViewController

@property (weak, nonatomic) id<FavouritesTableViewControllerDelegate>delegate;

@end
