//
//  FavouritesTableViewCell.h
//  WikiReader
//
//  Created by Maksims Moisja on 09/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserFavourite;

@interface FavouritesTableViewCell : UITableViewCell

- (void)setUserFavourite:(UserFavourite *)favourite;

@end
