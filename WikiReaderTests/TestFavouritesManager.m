//
//  TestFavouritesManager.m
//  WikiReader
//
//  Created by Maksims Moisja on 09/03/16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FavouritesManager.h"
#import "UserFavourite.h"

@interface TestFavouritesManager : XCTestCase

@property (strong, nonatomic) NSString *pathFavourites;

@end

@implementation TestFavouritesManager

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
	
	self.pathFavourites = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingString:@"/UserFavourites.plist"];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - FavouritesManager init
#pragma mark -

- (void)testFavouritesManagerInitsArray {
	XCTAssertNotNil([[FavouritesManager sharedManager] arrayOfFavourites]);
}

- (void)testFavouritesManagerInitsEmptyArrayWhenThereIsNoData {
	[self removeFavourites];
	[[FavouritesManager sharedManager] restoreFavourites];
	
	XCTAssert([[FavouritesManager sharedManager] arrayOfFavourites].count == 0);
}

- (void)testThatFavouritesManagerHasCorrectNumberOfObjectsInArrayAfterInit {
	[self removeFavourites];
	UserFavourite *uf = [[UserFavourite alloc] initWithTitle:@"test" pageURL:@"testURL"];
	[NSKeyedArchiver archiveRootObject:@[uf] toFile:self.pathFavourites];
	[[FavouritesManager sharedManager] restoreFavourites];
	
	XCTAssert([[FavouritesManager sharedManager] arrayOfFavourites].count == 1);
	
	UserFavourite *temp = [[FavouritesManager sharedManager] arrayOfFavourites].firstObject;
	XCTAssertEqualObjects(temp.pageTitle, @"test");
	XCTAssertEqualObjects(temp.pageURL, @"testURL");
}


#pragma mark - FavouritesManager saving
#pragma mark -

- (void)testThatFavouriteSavingWorks {
	UserFavourite *uf = [[UserFavourite alloc] initWithTitle:@"test1" pageURL:@"testURL1"];
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:uf]);
	XCTAssertTrue([[[FavouritesManager sharedManager] arrayOfFavourites] containsObject:uf]);
}

- (void)testThatFavouriteSavingWorksWithEqualDataObjects {
	UserFavourite *uf1 = [[UserFavourite alloc] initWithTitle:@"test2" pageURL:@"testURL2"];
	UserFavourite *uf2 = [[UserFavourite alloc] initWithTitle:@"test2" pageURL:@"testURL2"];
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:uf1]);
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:uf2]);
	XCTAssertTrue([[[FavouritesManager sharedManager] arrayOfFavourites] containsObject:uf1]);
	XCTAssertTrue([[[FavouritesManager sharedManager] arrayOfFavourites] containsObject:uf2]);
}

- (void)testThatFavouritesSavingWorkWithEqualObjects {
	UserFavourite *uf = [[UserFavourite alloc] initWithTitle:@"test3" pageURL:@"testURL3"];
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:uf]);
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:uf]);
	
	NSInteger pos1 = [[[FavouritesManager sharedManager] arrayOfFavourites] indexOfObject:uf];
	NSRange range = NSMakeRange(pos1+1, [[FavouritesManager sharedManager] arrayOfFavourites].count - (pos1 + 1));
	NSInteger pos2 = [[[FavouritesManager sharedManager] arrayOfFavourites] indexOfObject:uf inRange:range];
	
	XCTAssert(pos2 != NSNotFound);
}

- (void)testThatFavouritesSavingDoesNotCrashWhenSavingNilObject {
	[self removeFavourites];
	[[FavouritesManager sharedManager] restoreFavourites];
	XCTAssertFalse([[FavouritesManager sharedManager] saveFavouritePage:nil]);
	XCTAssertTrue([[FavouritesManager sharedManager] arrayOfFavourites].count == 0);
}


#pragma mark - FavouritesManager deleting
#pragma mark -

- (void)testThatDeletingObjectOnEmptyArrayDoesNotCrashApp {
	[self removeFavourites];
	[[FavouritesManager sharedManager] restoreFavourites];
	XCTAssertTrue([[FavouritesManager sharedManager] deleteFavouritePage:[[UserFavourite alloc] initWithTitle:@"t" pageURL:@"t"]]);
}

- (void)testThatDeletingUserFavouriteObjectWhichIsNotInArrayDoesNotCrashApp {
	[self removeFavourites];
	[[FavouritesManager sharedManager] restoreFavourites];
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:[[UserFavourite alloc] initWithTitle:@"a" pageURL:@"a"]]);
	XCTAssertTrue([[FavouritesManager sharedManager] deleteFavouritePage:[[UserFavourite alloc] initWithTitle:@"t" pageURL:@"t"]]);
}

- (void)testThatArrayIsUpToDateAfterDeletingObject{
	[self removeFavourites];
	[[FavouritesManager sharedManager] restoreFavourites];
	UserFavourite *uf = [[UserFavourite alloc] initWithTitle:@"a" pageURL:@"a"];
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:uf]);
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:[[UserFavourite alloc] initWithTitle:@"a" pageURL:@"a"]]);
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:[[UserFavourite alloc] initWithTitle:@"a" pageURL:@"a"]]);
	XCTAssertTrue([[FavouritesManager sharedManager] arrayOfFavourites].count == 3);
	
	XCTAssertTrue([[FavouritesManager sharedManager] deleteFavouritePage:uf]);
	XCTAssertTrue([[FavouritesManager sharedManager] arrayOfFavourites].count == 2);
	XCTAssertFalse([[[FavouritesManager sharedManager] arrayOfFavourites] containsObject:uf]);
}

- (void)testThatCorrectObjectIsDeletedIfDataOfMultipleObjectsIsEqual {
	[self removeFavourites];
	[[FavouritesManager sharedManager] restoreFavourites];
	UserFavourite *uf1 = [[UserFavourite alloc] initWithTitle:@"test2" pageURL:@"testURL2"];
	UserFavourite *uf2 = [[UserFavourite alloc] initWithTitle:@"test2" pageURL:@"testURL2"];
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:uf1]);
	XCTAssertTrue([[FavouritesManager sharedManager] saveFavouritePage:uf2]);
	XCTAssertTrue([[FavouritesManager sharedManager] deleteFavouritePage:uf1]);
	
	XCTAssertTrue([[[FavouritesManager sharedManager] arrayOfFavourites] containsObject:uf2]);
	XCTAssertFalse([[[FavouritesManager sharedManager] arrayOfFavourites] containsObject:uf1]);
}


#pragma mark - FavouritesManager restoring
#pragma mark -

- (void)testThatRestoringCreateEmptyArrayIfThereIsNoData {
	[self removeFavourites];
	[[FavouritesManager sharedManager] restoreFavourites];
	XCTAssertTrue([[FavouritesManager sharedManager] arrayOfFavourites].count == 0);
}

- (void)testThatRestoringCreatesArrayWithOneObjectIfFileContainsOneObject {
	[self removeFavourites];
	UserFavourite *uf = [[UserFavourite alloc] initWithTitle:@"t" pageURL:@"t"];
	[NSKeyedArchiver archiveRootObject:uf toFile:self.pathFavourites];
	
	[[FavouritesManager sharedManager] restoreFavourites];
	XCTAssertTrue([[FavouritesManager sharedManager] arrayOfFavourites].count == 1);
	XCTAssertEqualObjects(uf.pageTitle, ((UserFavourite *)[[FavouritesManager sharedManager] arrayOfFavourites].firstObject).pageTitle);
}

- (void)testThatRestoringCreatesArrayOfObjectsIfFileContainsArrayOfObjects {
	[self removeFavourites];
	UserFavourite *uf1 = [[UserFavourite alloc] initWithTitle:@"t1" pageURL:@"t1"];
	UserFavourite *uf2 = [[UserFavourite alloc] initWithTitle:@"t2" pageURL:@"t2"];
	UserFavourite *uf3 = [[UserFavourite alloc] initWithTitle:@"t3" pageURL:@"t3"];
	[NSKeyedArchiver archiveRootObject:@[uf1, uf2, uf3] toFile:self.pathFavourites];
	
	[[FavouritesManager sharedManager] restoreFavourites];
	XCTAssertEqualObjects(uf1.pageTitle, ((UserFavourite *)[[FavouritesManager sharedManager] arrayOfFavourites][0]).pageTitle);
	XCTAssertEqualObjects(uf2.pageTitle, ((UserFavourite *)[[FavouritesManager sharedManager] arrayOfFavourites][1]).pageTitle);
	XCTAssertEqualObjects(uf3.pageTitle, ((UserFavourite *)[[FavouritesManager sharedManager] arrayOfFavourites][2]).pageTitle);
	XCTAssertTrue([[FavouritesManager sharedManager] arrayOfFavourites].count == 3);
}


#pragma mark - Helper methods
#pragma mark -

- (void)removeFavourites {
	if ([[NSFileManager defaultManager] fileExistsAtPath:self.pathFavourites]) {
		[[NSFileManager defaultManager] removeItemAtPath:self.pathFavourites error:nil];
	}
}

@end
